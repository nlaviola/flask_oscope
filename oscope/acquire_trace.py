# -*- coding: utf-8 -*-
import visa, sys, json
from pathlib import Path

def acquire_trace(file_path, tool_address, record_length, channel_str):

  # file_path = sys.argv[1]
  # tool_address = sys.argv[2]
  # record_length = sys.argv[3] # user has to define points on GUI / label as "Record Length"
  # split channel string into a list
  channels = channel_str.split(',')

  rm = visa.ResourceManager('@py')
  
  #attempt connection to O-scope
  try:
    tek = rm.open_resource(tool_address)
  except Exception:
    #print(channel_dict)
    return Exception
    sys.exit(1) #exit program if connection is unsuccessful
  except visa.Error:
    #print(channel_dict)
    return visa.Error
    sys.exit(1) #exit program if connection is unsuccessful

  #set device timeout
  tek.timeout = 25000
  device_id = tek.query("*IDN?")

  # name file to write data
  f = open(file_path, "a")

  for channel in channels:
    channel_str = ":DAT:SOU CH{}".format(channel)
    #channel_str = ":DAT:SOU CH1"
    #print channel_str
    tek.write(channel_str) #update channel
    tek.write(":WFMO:BIT_N 16")
    tek.write(":DATA:ENC ASCI")
    tek.write(":DAT:START 1")
    tek.write(":DAT:STOP " + str(record_length))

    waveform = tek.query("CURV?")

    tek.write("HEAD 0") # This is important to be set to 0 if we do not want the Oscope to respond with the variable name and then the value.

    xze = tek.query(":WFMO:XZE?")
    xin = tek.query(":WFMO:XIN?")
    yze = tek.query(":WFMO:YZE?")
    ymu = tek.query(":WFMO:YMU?")
    yof = tek.query(":WFMO:YOF?")

    f.write(waveform + "\n")
    f.write(xze + xin + yze + ymu + yof + "\n")

  f.close()

  #check for the presence of the file, return true if it exists
  file = Path(file_path)
  if file.is_file():
    return True
  else:
    return False
