import subprocess
import platform

# ping IP address and return ture if availabel and false if not
def ping_ip(current_ip_address):
  try:
    output = subprocess.check_output("ping -{} 1 {}".format('n' if platform.system().lower() == "windows" else 'c', current_ip_address ), shell=True, universal_newlines=True)
    if 'unreachable' in output:
      return False
    else:
      return True
  except Exception:
    return False
