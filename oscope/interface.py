from flask import (
  abort, Blueprint, flash, g, jsonify, redirect, render_template, request, send_file, session, url_for
)
from flask import current_app
from oscope.db import get_db
from oscope.utils import ping_ip
from oscope.tool_state_check import tool_state_check
from oscope.acquire_trace import acquire_trace

import csv, os, re, sys, time

#increase the max field size for large CSVs
csv.field_size_limit(sys.maxsize)

bp = Blueprint('interface', __name__, url_prefix='/interface')

@bp.route('/dashboard')
def dashboard():
  db = get_db()
  tools = db.execute(
    'SELECT * FROM tools'
  ).fetchall()
  latest_trace = db.execute(
    'SELECT * FROM traces'
    ' ORDER BY unix_timestamp DESC'
    ' LIMIT 1'
  ).fetchall()

  return render_template('interface/dashboard.html', tools=tools, latest_trace=latest_trace)

@bp.route('/download_trace_csv/<unix_timestamp>')
def download_trace_csv(unix_timestamp):
  if unix_timestamp:
    db = get_db()
    # get the trace information
    trace_information = db.execute(
      'SELECT * FROM traces'
      ' WHERE unix_timestamp = ?', (unix_timestamp,)
    ).fetchone()
    # remove whitespace for filename
    trace_name = trace_information['name'].replace(" ", "")
    #remove characters that could break the filename
    trace_name = re.sub(r'[^0-9a-zA-Z]+', '', trace_name)

    if len(trace_name) > 0:
      trace_name = trace_name + "_"

    #set the filepath for download
    file_path = f"uploads/oscilloscope_traces/{unix_timestamp}.csv"
    file = os.path.join(current_app.root_path, file_path)

    #download if the file exists, otherwise show an error
    return send_file(file, as_attachment=True, attachment_filename=trace_name + unix_timestamp + '.csv')
  else:
    abort(404)
  

@bp.route('/fill_trace_table_ajax')
def fill_trace_table_ajax():
  if request.method == 'GET':
    # query for all trace information
    db = get_db()
    traces = db.execute(
      'SELECT * FROM traces'
      ' ORDER BY unix_timestamp DESC'
    ).fetchall()

    table_data = {
      'data': traces
    }

    return jsonify(table_data)

@bp.route('/get_tool_status_ajax', methods=['POST'])
def get_tool_status_ajax():
  if request.method == 'POST':
    ip_address = request.form['ip_address']
    tool_address = request.form['tool_address']

    # ping IP address
    status = ping_ip(ip_address)

    #if online, get channel status info
    if status:
      tool_state = tool_state_check(tool_address)
      return jsonify(tool_state)

    if not status:
      return jsonify(tool_up = False)

@bp.route('/get_trace_data_ajax', methods=['POST'])
def get_trace_data_ajax():
  if request.method == 'POST':
    db = get_db()
    unix_timestamp = request.form['unix_timestamp']
    # query for latest trace data
    trace_information = db.execute(
      'SELECT * FROM traces'
      ' WHERE unix_timestamp = ?', (unix_timestamp,)
    ).fetchone()

    #grab file contents
    file_path = f"uploads/oscilloscope_traces/{unix_timestamp}.csv"
    file = os.path.join(current_app.root_path, file_path)
    csv_data=[]
    with open(file, "r") as csvfile:
      file_contents= csv.reader(csvfile, delimiter=' ')
      for row in file_contents:
        if len(row) != 0:
          csv_data.append(row)

    ajax_data = {}
    ajax_data['trace_information'] = trace_information
    ajax_data['csv_data'] = csv_data

    return jsonify(ajax_data)

@bp.route('/trace_capture', methods=['POST'])
def trace_capture():
  if request.method == 'POST':
    db = get_db()

    trace_form_information = {}
    trace_form_information['oscilloscope'] = request.form['oscilloscope']
    trace_form_information['record_length'] = request.form['record_length']
    trace_form_information['channel_select'] = request.form.getlist('channel_select')
    #convert channel_select list to csv string
    trace_form_information['channel_select'] = ",".join(trace_form_information['channel_select'])
    selected_handle = request.form['oscilloscope']
    #get the selected tool info
    tool = db.execute(
      'SELECT * FROM tools'
      ' WHERE handle = ?', (selected_handle,)
    ).fetchone()
      
    return render_template('interface/loading.html', tool=tool, trace_form_information=trace_form_information)

@bp.route('/trace_store_ajax', methods=['POST'])
def trace_store_ajax():
  if request.method == 'POST':
    db = get_db()

    #get form parameters
    record_length = request.form['record_length']
    tool_handle = request.form['oscilloscope']
    channel_str = request.form['channel_select']

    #query for tool information
    tool = db.execute(
      'SELECT * FROM tools'
      ' WHERE handle = ?', (tool_handle,)
    ).fetchone()

    #get the current unix timestamp
    unix_timestamp = int(time.time())

    #create path to upload file
    file_path = f"uploads/oscilloscope_traces/{unix_timestamp}.csv"
    file = os.path.join(current_app.root_path, file_path)

    #run acquire trace script
    file_status = acquire_trace(file, tool['instr_address'], record_length, channel_str)

    trace_success = False

    if file_status:
      trace_success = True
      #update channel str format
      channel_str = "[" + channel_str + "]"
      #save trace information to database
      db.execute(
        'INSERT INTO traces (name, purpose, channels, record_length, unix_timestamp)'
        ' VALUES (?, ?, ?, ?, ?)',
        (tool_handle, '...', channel_str, record_length, unix_timestamp)
      )
      db.commit()

    return jsonify(trace_success)


@bp.route('/update_trace_information_ajax', methods=['POST'])
def update_trace_information_ajax():
  if request.method == 'POST':
    parameters = request.get_json()
    #get the row data
    row_data = parameters['data']

    # extract dat into single point lists
    row_id = list(row_data.keys())
    data_key = list(row_data[row_id[0]].keys())
    data_value = row_data[row_id[0]][data_key[0]]

    #update data
    db = get_db()
    db.execute(
        'UPDATE traces SET "{}" = ? WHERE id = ?'.format(data_key[0].replace('"', '""')),
        (data_value, row_id[0])
    )
    db.commit()

    # format data to return to datatables
    #must be a dict of a list of a dict e.g {data: [{key:value, key2:value2}]}
    update_data = {
      'id': row_id[0],
      data_key[0]: data_value
    }
    data_list = []
    data_list.append(update_data)
    return_data = {
      'data': data_list
    }

    return return_data






