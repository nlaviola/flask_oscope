//* ***Javascript functions for the Testing app*****//
//* ***Sorted alphabetically*****//
// namespace all functions for organization and to encapsulate functions

// top level
var TestingApp = {}

TestingApp.testMaker = {

  /**
	 * fills and displays the add field modal
   *
	 */
  displayAddFieldModal: function(){
		$('a.add-field').on('click', function (e) {
			//get data attributes from link
			var dataAttributes = $(this).data()

			// fill in modal
			$('#addFieldModalLabel').html('Add Field to <strong>' + dataAttributes.icon + ' ' + dataAttributes.tableName + '</strong>')
			$('#table_name').val(dataAttributes.tableName);

			// display modal
			$('#addFieldModal').modal('show')
		})
  },

  /**
	 * Loads all tests into a datatable along with links to test pages
   *
   * @param    baseUrl   input string
	 */
   loadAllTestsDataTable: function (baseUrl){

     $.ajax({
 	        type: 'GET',
 	        dataType: 'json',
 	        url: baseUrl + 'api/test_maker_api/tests/',
 	        success: function(res) {

     				//prep columns array
     				var columns = [
              {'data': 'id'},
              {'data': 'test_name',
                "render": function ( data, type, full, meta ) {
                   return '<a href="/test_maker/manage_test/'+ full['id'] +'">' + data +'</a>';
                }
              },
              {'data': 'test_purpose'},
              {'data': 'date_generated'}
            ]

            console.log(res)

            var tableHeader = "<th>Id</th><th>Test Name</th><th>Purpose</th><th>Date Generated</th>"

     				//write table headers
     				$('#allTestsTableDiv').html('<table class="table table-bordered table-striped text-gray-800" id="allTestsDataTable" width="100%" cellspacing="0"><thead><tr>' + tableHeader + '</tr></thead></table>')

     				//initialize the datatable and feed data to it
     	      $('#allTestsDataTable').DataTable({
       	        data: res.data,
       	        columns: columns,
       					"lengthMenu": [ 10, 25, 50 ],
       				  "order": [[ 0, "desc" ]]
     	        });
     	    },
       		error: function(res){

       				$('#allTestsTableDiv').html(res.responseJSON['message'])

       		}
 	    })
   },

  /**
	 * Loads data into datatables
   *
   * @param    baseUrl   input string
	 * @param    testId   input string
	 */
   loadTestMakerDataTable: function (baseUrl, testId){

     $.ajax({
 	        type: 'GET',
 	        dataType: 'json',
 	        url: baseUrl + 'api/test_maker_api/custom_data/' + testId,
 	        success: function(res) {

            //request was successful, but no data returned
            if (res.message) {
              $('#dataTableDiv').html('<div class="alert alert-warning" role="alert">' + res.message + '</div>')
            } else {

              //prep columns array
       				var columns = []
              var tableHeader = ""
              $.each(res.columns, function (key, value) {
                 columns.push({ "data": value.name })
                 tableHeader += "<th>" + value.name + "</th>"
              })

       				//write table headers
       				$('#dataTableDiv').html('<table class="table table-bordered table-striped text-gray-800" id="testMakerDataTable" width="100%" cellspacing="0"><thead><tr>' + tableHeader + '</tr></thead></table>')

       				//initialize the datatable and feed data to it
       	      $('#testMakerDataTable').DataTable({
         	        data: res.data,
         	        columns: columns,
         					"lengthMenu": [ 5, 10, 25 ],
         				  "order": [[ 0, "desc" ]]
       	      })

            }


     	    },
   			  error: function(res){

   				  $('#dataTableDiv').html(res.message)

   			  }
 	    })
   },

   removeField: function(baseUrl){

     //remove a field
 		$('a.delete-field-link').on('click', function(){

 		  //get data attributes from link
   		var dataAttributes = $(this).data()

 		  var warningMessage = "Warning! You are about to delete the *" + dataAttributes.field_name + "* field. This operation cannot be undone and any data contained in this field will be permanently lost. Do you wish to continue?"

 		  //exit script if user selects Cancel
 		  if (!confirm(warningMessage)){
 		  	return false
 		  }

 		  //send to controller for field deletion
 		  $.post(baseUrl + 'test_maker/remove_table_field_ajax', dataAttributes)

 		  .then(function (data) {

 			var response = JSON.parse(data)

   			//reload the page to display the flashdata to the user
   	    if (response['success'] === true || response['success'] === false) {
   	      window.location.reload(true)
   	    }
   			else
   			{
   			  alert('Something went wrong! Please contact the Web Administrator!')
   			  window.location.reload(true)
   			}

 		  })

 		})

  },

  /**
 	 * Submits new field data
   *
   * @param    baseUrl   input string
 	 */
   submitAddFieldForm: function (baseUrl){

     $('form#add_field_form').on('submit', function (e) {
 		    // stop the form from submitting
 	      e.preventDefault()

 		    // get values from form
 	      var addFieldValues = $('#add_field_form').serializeArray()

 		    // initialize data object
 	      dataObj = {}

 		    // create object from serialized array
 	      $(addFieldValues).each(function (i, field) {
 			      dataObj[field.name] = field.value
 	      })

 		    // send to controller
 	      $.post(baseUrl + 'test_maker/add_field_to_table_ajax', dataObj)

 		    .then(function (data) {

   	      var response = JSON.parse(data)

   			  //reload the page to display the flashdata to the user
   	      if (response['success'] === true || response['success'] === false) {
   	        window.location.reload(true)
   	      } else {
   			    alert('Something went wrong! Please contact the Web Administrator!')
   			    window.location.reload(true)
   			  }

 	      })

 	   })

   }

 }

 TestingApp.yokoOsa = {

    /**
   	 * Loads and sets up the datatable for the yokoOSA trace feature
     *
     * @param    baseUrl   input string
   	 */
    loadYokoDatatable: function(baseUrl){

      //load outer state into function to access other function in namespace
      that = this

      // Data Tables
  		let editor; // use a global for the submit and return data rendering in the examples

  	    editor = new $.fn.dataTable.Editor( {
  	        //set url to update fields for inline editing
  			"ajax": baseUrl + "/yoko_osa/update_trace_information",
  		   	"table": "#traceTable",
  			//set the id source
  		   	"idSrc": "id",
  			//decalre fields to be edited
  		   	"fields": [ {
  				   "label": "Name",
  				   "name": "name"
  			   }, {
  				   "label": "Purpose",
  				   "name": "purpose"
  			   }
  		   	]

  	    });

  	    // Activate an inline edit on click of a table cell
  	    $('#traceTable').on( 'click', 'tbody td:not(:first-child, :nth-child(4), :nth-child(5), :last-child)', function (e) {
  	        editor.inline(this);
  	    } );

  	    let traceTable = $('#traceTable').DataTable( {
  	        "dom": "Bfrtip",
  			//data source for table
  			"ajax": {
  	            "url": baseUrl + "yoko_osa/fill_trace_table_ajax",
  	            "type": "GET"
  	        },
  			//set the table order by the timestampt (the 4th column)
  			"order": [[ 4, "desc" ]],
  			"initComplete": function(settings, json) {
  				//bind event to render links, existing and future
  				$(document).on('click', 'a.plotCSVData', function(){

  					let link = $(this)
  					let selectedUnixTimestamp = link.data('unixTimestamp')

  					that.renderTrace(baseUrl, selectedUnixTimestamp)

  				})

  				//add renderedRow class to latest timestamp row if it is not already added
  				row = $('#traceTable tbody tr:first-child');
  				if (!$(row).hasClass('renderedRow')) {
  					$(row).addClass('renderedRow');
  				}

  			},
  			rowCallback: function( row, data, dataIndex ) {
  				//add data attribute with unix timestamp to each row once it is created
  	      		$(row).attr('data-unix-timestamp', data.unix_timestamp);

  				$('td:eq(5)', row).html('<a href="' + baseUrl + 'yoko_osa/download_trace_csv/' + data.unix_timestamp + '" class="traceDownload"><i class="fas fa-fw fa-file-csv"></i></a> <a data-unix-timestamp="' + data.unix_timestamp + '" href="javascript:void(0);" class="plotCSVData"><i class="fas fa-fw fa-chart-area"></i></a>');

  				//bind click event to plot icon
  				$('a.plotCSVData').off().on('click', function(){

  					let link = $(this)
  					let selectedUnixTimestamp = link.data('unixTimestamp')

  					that.renderTrace(baseUrl,selectedUnixTimestamp)

  				})
  			},
  	        "columns": [
  	            { data: "id" },
  	            { data: "name" },
  	            { data: "purpose" },
  	            { data: "unix_timestamp" },
  	            { data: "date_generated" },
  				{
  					data: null
  				},
  	        ],
  	        "select": {
  	            style:    'single',
  	            selector: 'td:first-child'
  	        },
  	        buttons: [
  	            //{ extend: "remove", editor: editor }
  	        ]
  	    });

    },

    /**
   	 * renders the Apex Chart for a given timestamp trace
     *
     * @param    baseUrl          input string
     * @param    unixTimestamp    input string
   	 */
    renderTrace: function (baseUrl, unixTimestamp){

      //empty the target div of any content
  		//this clears any previously rendered charts
  		$("div#chartParent").empty()

  		//rewrite target div
  		$("div#chartParent").html('<div id="yokoChart" class="px-4 pb-4"></div>')

  		//get trace data for a given timestamped file
  		$.post(baseUrl + "yoko_osa/get_trace_data_ajax", {unix_timestamp: unixTimestamp})
  	    .then(function(traceData){

  			dataObj = JSON.parse(traceData)

  			traceInformation = dataObj['trace_information']

  			//create an array from the csv data
  			csvArr = dataObj['csv_data'].split('\n');

  			let tracePlotDataObj = [];
  			for(let i = 1; i < csvArr.length; i++) {
  			  let pieces = csvArr[i].split(',');

  			  let dataEntry = {}

  			  //cut off data to 3 sig digits
  			  if (pieces[0].length > 0 && pieces[1].length > 0) {
  				  //convert strings to numbers
  				  xVal = Number(pieces[0])
  				  yVal = Number(pieces[1])
  				  //keep up to firsy 3 digits after decimal
  				  dataEntry.x = Number(xVal.toFixed(3))
  				  dataEntry.y = Number(yVal.toFixed(3))

  				  tracePlotDataObj.push(dataEntry);
  			  }

  			}

  			//set Chart Title
  			let titleText
  			if (traceInformation[0].name.length == 0) {
  				titleText = traceInformation[0].date_generated
  			} else {
  				titleText = traceInformation[0].name + ' - ' + traceInformation[0].date_generated
  			}

  			//remove noise in trace data for plot
  	        let scatArr = []

  	        //formatting data for chart
  	        $.each(tracePlotDataObj, function(key, data){
  				if (tracePlotDataObj[key].y > -120) {
  					scatArr.push(tracePlotDataObj[key])
  				}
  			})

  			//set chart options
  			let options = {
  			    series: [{
  			          name: "Trace",
  			          data: scatArr
  			        }],
  			    chart: {
  			          height: 350,
  			          type: 'scatter',
  			          zoom: {
  			            enabled: true,
  			            type: 'xy'
  					},
  					events: {
  					    mounted: function(chartContext, config) {
  							//Remove the CSV download link from the chart after the chart is mounted
  							$('div.apexcharts-menu-item.exportCSV').remove()
  					    }
  					}
  			    },
  				title: {
  				    text: titleText,
  				},
  			    xaxis: {
					  type: 'numeric', //fixes issue with tick rendering in different browsers
  					  tickAmount: 7,
  					  tooltip: {
  					      formatter: function(val, opts) {
  					        return val
  					      }
  					  }
  			    },
  			    yaxis: {
  			          tickAmount: 7
  				},
  				markers: {
  				    size: 3,
  				}
  			};

  			let chart = new ApexCharts(document.querySelector("#yokoChart"), options);

  			chart.render();

  			//once the chart has rendered, highlight the particular row of data rendered
  			let tableRows = $('#traceTable tbody tr')

  			tableRows.removeClass('renderedRow')

  			$('#traceTable tbody tr[data-unix-timestamp="' + unixTimestamp + '"]').addClass('renderedRow')

  	    })

	},
	
	 /**
   	 * pings Yoko OSAs in the office and reports back with a status update
     *
     * @param    baseUrl    input string
     * @param    tools    	input array
   	 */
	updateToolStatus: function (baseUrl, tools){

		$.each(tools, function (i, tool) {

			//check online status of each tool
			$.post(baseUrl + 'yoko_osa/ping_tool_ajax', {ip_address: tool.ip_address}, function(data){
	
				toolStatus = JSON.parse(data)
	
				//update tool status box
				if (toolStatus.status == 'online') {
					$("#" + tool.handle + "-osa-status").html('<div><span class="text-success">ONLINE<span></div><div><span><small>' + tool.ip_address + '</small><span></div>')
				}
	
				if (toolStatus.status == 'offline') {
					$("#" + tool.handle + "-osa-status").html('<div><span class="text-danger">OFFLINE<span></div><div><span><small>' + tool.ip_address + '</small><span></div>')
					//disbale the select option
					$('#osa option[value=' + tool.handle + ']').prop('disabled', true);
				}
	
			})
	
		})

	}

} //END: TestingApp.yokoOsa = {
