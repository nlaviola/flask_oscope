Vue.component('tool-card', {

  props: ['title', 'description', 'toollink', 'img'],

  template: `
  <div class="card mb-2" style="width: 18rem;">
    <img :src="img" class="card-img-top" style="height: 14rem;" alt="...">
    <div class="card-body" style="height: 10rem;">
      <h5 class="card-title">{{ title }}</h5>
      <p class="card-text" >{{ description }}</p>
    </div>
    <div class="modal-footer">
      <a :href="toollink" class="btn btn-primary">View tool</a>
    </div>
  </div>
  `


})

new Vue({

  el: '#root'

})
