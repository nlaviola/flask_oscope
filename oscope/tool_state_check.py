# -*- coding: utf-8 -*-
import pyvisa as visa, sys, json
from visa import VisaIOError

def tool_state_check(tool_address):

  channel_dict = {}
  channel_dict['tool_up'] = False

  rm = visa.ResourceManager('@py')

  #attempt connection to O-scope
  try:
    tek = rm.open_resource(tool_address)
  except Exception:
    #print(channel_dict)
    return Exception
    sys.exit(1) #exit program if connection is unsuccessful
  except visa.Error:
    #print(channel_dict)
    return visa.Error
    sys.exit(1) #exit program if connection is unsuccessful

  #convert unicode response to string
  device_id = str(tek.query("*IDN?")).split(',')
  

  if device_id[0] == 'TEKTRONIX':
    channel_dict['tool_up'] = True

  # set number of channels to check for probe presence
  channels = [1, 2, 3, 4]

  for i in channels:

    #build query string
    channel_str  = "SEL:CH{}?".format(i)

    # check channel status
    channel_status = tek.query(channel_str)

    #convert unicode response to string and remove whitespace
    response = str(channel_status).strip()

    if response == '1':
      channel_dict['channel_{}'.format(i)] = True
    if response == '0':
      channel_dict['channel_{}'.format(i)] = False

  return channel_dict
